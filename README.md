# seat-select (vue移动端座位选择)

声明致谢：本项目基于开源项目(月影银翔 / seat-select )二次优化与迭代开发，源项目：<https://gitee.com/tanxiang/seat-select>，在此对源项目作者表示致谢！！！

### 座位图体系项目导航

- [移动端 seat-select](https://xunyin.top/public/upload/images/20230301/1677653875.jpg)

> 此次项目是基于vue编写的类似某票票和某眼的电影`移动端`锁座页面,超过上万个影厅的测试,其中包含功能
>
> - 座位图生成
> - 座位预览图生成
> - 座位`留空`检测
> - 座位智能选择`最优座位`算法
> - 自适应影厅大小
> - 座位图左侧导航栏的过道检测
> - 普通座位的选择逻辑
> - 情侣座位的选择逻辑
> - 锁座提示

#### 项目截图

<img src="https://xunyin.top/public/upload/images/20230301/1677653875.jpg?raw=true" width="640" hegiht=""/>

#### 之后的计划

暂无

### 赞助者名单

暂无

#### 智能选座示例

```
以下为多个影厅的智能选座gif图演示
```

暂无

#### 智能选座示例

```
以下为空位检测逻辑gif图演示
```

暂无

### 项目依赖组件

该项目引用到的外部常用组件

> 1. [amfe-flexible](https://github.com/amfe/lib-flexible "阿里巴巴弹性rem布局")
>
> 2. [vue-touch](https://github.com/vuejs/vue-touch "vue-touch")
>
> 3. [better-scroll](https://github.com/ustbhuangyi/better-scroll "better-scroll")
>
> 4. [px2rem-postcss](https://github.com/songsiqi/px2rem-postcss "px2rem-postcss")
>
> 5. [axios(不是必须,可替换成其他请求组件)](https://github.com/axios/axios "axios(不是必须,可替换成其他请求组件)")

```
*  vue-touch需要使用@next分支

npm install vue-touch@next -S

npm install amfe-flexible -S

npm install postcss-px2rem -S

npm install better-scroll -S

npm install axios -S
```

### 目录结构

```
.public
├── index.html
└── mock
    └── seatLove.json(mock数据在这里)

.src
├── App.vue --(入口组件)
├── assets --(静态文件)
│   ├── images
│   │   └── loading.gif --(加载图片)
│   └── stylus
│       ├── golbal.styl --(全局styl)
│       └── reset.styl --(移动端重写CSS)
├── components --(公用组件)
│   ├── Header.vue --(头部公用组件)
│   └── loading.vue --(载入公用组件)
├── http.js (axios工具类)
├── main.js (vue入口js)
├── pages --(组件目录)
│   └── hallseat
│       ├── HallSeat.vue --(选座父组件)
│       └── component --(选座子组件目录)
│           ├── ConfirmLock.vue --(确认选择组件)
│           ├── PlanDetail.vue --(电影信息组件)
│           ├── QuickSelectTab.vue --(智能选座组件)
│           ├── SeatArea.vue --(座位图生成组件)
│           └── SelectedTab.vue --(已选座展示组件)
│           └── Tips.vue --(自动调座提示组件)
└── router.js
```

### 跑跑试试看,本地运行此项目 (Project setup,Compiles and hot-reloads for development)

```
下载代码后在文件目录下运行命令

1. npm install 安装全部依赖

(cnpm install 或者 yarn install)

2. npm run serve 运行到开发环境
```

### 项目打包 (Compiles and minifies for production)

```
npm run build
```

### 讨论

### 项目捐赠

写代码不易...请作者喝杯咖啡呗?

### 其他项目

<https://xunyin.top>
